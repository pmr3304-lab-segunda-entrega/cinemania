from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .temp_data import movie_data
from django.urls import reverse
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.views import generic


class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'movies/detail.html'
    context_object_name = 'movie'


class PostListView(generic.ListView):
    model = Post
    context_object_name = 'movie_list'
    template_name = 'movies/index.html'


def search_movies(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        movie_list = Post.objects.filter(name__icontains=search_term)
        context = {"movie_list": movie_list}
    return render(request, 'movies/search.html', context)


class PostCreateView(generic.CreateView):
    context_object_name = 'movie'
    form_class = PostForm
    model = Post
    template_class = PostForm
    template_name = "movies/create.html"

    def get_success_url(self):
        return reverse('movies:detail', kwargs={'pk': self.object.id})


class PostUpdateView(generic.UpdateView):
    context_object_name = 'movie'
    form_class = PostForm
    model = Post
    template_class = PostForm
    template_name = "movies/update.html"

    def get_success_url(self):
        return reverse('movies:detail', kwargs={'pk': self.object.id})


class PostDeleteView(generic.DeleteView):
    model = Post
    context_object_name = 'movie'
    template_name = 'movies/delete.html'
    
    def get_success_url(self):
        return reverse('movies:index')


def create_comment(request, movie_id):
    movie = get_object_or_404(Post, pk=movie_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                              text=comment_text,
                              movie=movie)
            comment.save()
            return HttpResponseRedirect(
                reverse('movies:detail', args=(movie_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'movie': movie}
    return render(request, 'movies/comment.html', context)


class CategoryListView(generic.ListView):
    model = Category
    template_name = 'movies/categories.html'


def list_category_movies(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'movies/category_individual.html', context)