from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255)
    release_year = models.IntegerField()
    director = models.CharField(max_length=255)
    poster_url = models.URLField(max_length=255, null=True)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.name} ({self.release_year})'


class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    movie = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    movies = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'